import { Component, OnInit, AfterViewInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
declare var TagCanvas: any;

@Component({
  selector: 'word-cloud',
  template: `<div id="word-cloud" *ngIf="hashtagsData">
                <div *ngIf="success">
                    <canvas id="myCanvas" height="300" width="700">
                        <ul>
                            <li *ngFor="let data of hashtagsData; let ind = index">
                                <a attr.data-weight="{{data.weight}}"
                                href="javascript:void(0)" 
                                (click)="clickTag(data)">{{data.trending}}
                                </a>
                            </li>
                        </ul>
                    </canvas>
                </div>
            </div>`,
  styles: [`#myCanvas {
                display: block;
                background-color: white;
            }

            #word-cloud {
                display: block;
                align-content: center;
                margin-left: 30px;
                margin-right: auto;
            }

            .right-click {
                background-color: lightgray;
            }

            .right-click:hover {
                background-color: lightslategray;
                cursor: pointer;
            }

            a {
                display: block;
                /*margin: auto;*/
            }`]
})

export class BaseWidgetWordCloudComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() hashtagsData: any = [
    {
      weight: 10,
      trending: 'Us'
    }, {
      weight: 20,
      trending: 'Donald Trump'
    }, {
      weight: 30,
      trending: 'Jokowi'
    }, {
      weight: 30,
      trending: 'Ahok'
    }, {
      weight: 20,
      trending: 'Basuki'
    }, {
      weight: 35,
      trending: 'Anies'
    }, {
      weight: 36,
      trending: 'Djarot'
    }, {
      weight: 35,
      trending: 'Indonesia'
    }, {
      weight: 35,
      trending: 'Malaysia'
    }, {
      weight: 10,
      trending: 'Pemilu'
    }, {
      weight: 30,
      trending: 'Pilkada'
    }, {
      weight: 40,
      trending: 'Jakarta'
    }, {
      weight: 30,
      trending: 'Obama'
    }];

  @Input() shape: any;
  @Input() maxData: any;
  @Output() selectedData = new EventEmitter;
  public hashtagResult: any;
  public elementId = 'word-cloud';
  public success = true;

  constructor(
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.loadWordCloud();
  }

  ngOnChanges() {
    // this.loadWordCloud();
  }

  loadWordCloud() {
    // try {
    TagCanvas.textColour = '#0277BD';
    TagCanvas.outlineColour = '#FFF';
    TagCanvas.dragControl = true;
    TagCanvas.weight = true;
    TagCanvas.weightMode = 'both';
    TagCanvas.shape = (this.shape !== undefined || this.shape !== null) ? this.shape : 'sphere';
    TagCanvas.weightFrom = 'data-weight';
    TagCanvas.Start('myCanvas');
    // } catch (e) {

    //   this.success = !this.success;
    // }
  }

  clickTag(data: any) {
    this.selectedData.emit(data);
  }

}
