import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  data = [
      {
        weight: 10,
        trending: 'Us'
      }, {
        weight: 20,
        trending: 'Dt'
      }, {
        weight: 30,
        trending: 'Jokowi'
      }, {
        weight: 30,
        trending: 'Ahok'
      }, {
        weight: 20,
        trending: 'Basuki'
      }, {
        weight: 35,
        trending: 'Anies'
      }, {
        weight: 36,
        trending: 'Djarot'
      }, {
        weight: 35,
        trending: 'Indonesia'
      }, {
        weight: 35,
        trending: 'Malaysia'
      }, {
        weight: 10,
        trending: 'Pemilu'
      }, {
        weight: 30,
        trending: 'Pilkada'
      }, {
        weight: 40,
        trending: 'Jakarta'
      }, {
        weight: 30,
        trending: 'Obama'
      }];
}
