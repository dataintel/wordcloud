import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetWordCloudComponent } from './src/word-cloud.component';

export * from './src/word-cloud.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetWordCloudComponent,
  ],
  exports: [
    BaseWidgetWordCloudComponent,
  ]
})
export class WordCloudModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: WordCloudModule,
      providers: []
    };
  }
}
